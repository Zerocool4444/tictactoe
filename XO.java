import java.util.Scanner;

public class XO {

	public static char[][] board;
	public static int row;
	public static int col;
	public static boolean win = false;
    public static boolean draw = false;
	public static char player = 'X';

	public static void main(String[] args) {
		init(3);
        printWelcome();
		while (!win) {
			display(board);
            showTurn(player);
			input(player, board);
			draw = checkDraw(board);
			win = checkWin(board);
			if (win || draw) {
				display(board);
				winPlayer(player, win);
				break;
			}
			player = switchPlayer(player);
		}
	}

    public static void init(int rowCol) {
        row = rowCol;
        col = rowCol;
        board = new char[row][col];

        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board.length; col++) {
                board[row][col] = ' ';
            }
        }
    }

    public static void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void winPlayer(char player, boolean win) {
        if (win) {
            System.out.println(player + " Win");
        } else {
            System.out.println("Player Draw!");
        }
    }

    public static void display(char[][] board) {
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[row].length; col++) {
                System.out.print(" " + board[row][col] + " ");
                if (col == 0 || col == 1) {
                    System.out.print("|");
                }
            }
            if (row == 0 || row == 1) {
                System.out.print("\n-----------\n");
            }
        }
        System.out.print("\n");
    }

    public static void showTurn(char player) {
        System.out.println("Turn " + player);
    }

    public static void input(char player, char[][] board) {
        Scanner input = new Scanner(System.in);
        System.out.print("Please input row, col: ");
        try {
            int inputX = input.nextInt();
            int inputY = input.nextInt();
            if (inputX > 0 && inputX < 4 && inputY > 0 && inputY < 4)
                updateSlot(inputX - 1, inputY - 1, player, board);
			else{
				System.out.println("Error you can input more than or equal 1 and less than or equal 3");
				afterError(player, board);
			}
        } catch (Exception e) {
            System.out.println("Error input isn't numeric");
            afterError(player, board);
        }
    }

	public static char switchPlayer(char player) {
        return (player == 'X') ? 'O' : 'X';
    }

	public static void updateSlot(int row, int col, char player, char[][] board) {
		if (checkSlot(row, col, board)) 
			board[row][col] = player;
		else{
			System.out.println("Error Choose a duplicate position");
            afterError(player, board);
		}
	}

    public static boolean checkSlot(int row, int col, char[][] board) {
		return (board[row][col] == ' ') ? true : false ;
	}

    public static void afterError(char player, char[][] board){
        display(board);
        showTurn(player);
        input(player, board);
    }

    public static boolean checkDraw(char[][] board) {
        int count = 0;
        for (char[] array : board) {
            for (char val : array) {
                count = (val == 'X' || val == 'O') ? count + 1 : count;
            }
        }
        return (count == 9) ? true : false;
    }

	public static boolean checkWin(char[][] board) {
 		return (checkRow(board)) ? true : (checkCol(board)) ? true : (checkCross(board)) ? true : false;
	}

    public static boolean checkRow(char[][] board) {
        for (int row = 0; row < board.length; row++) {
            if (board[row][0] == board[row][1] && board[row][1] == board[row][2] && board[row][0] != ' ') {
                return true;
            }
        }
        return false;
    }

    public static boolean checkCol(char[][] board) {
        for (int col = 0; col < board[0].length; col++) {
            if (board[0][col] == board[1][col] && board[1][col] == board[2][col] && board[0][col] != ' ') {
                return true;
            }
        }
        return false;
    }

    public static boolean checkCross(char[][] board) {
    	return ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ')
                || (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != ' '))
    			? true : false;
    }
}